//
//  ViewController.swift
//  blow
//
//  Created by Ihwan ID on 13/06/20.
//  Copyright © 2020 Ihwan ID. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import CoreAudio

class ViewController: UIViewController {

    @IBOutlet weak var tulisan: UILabel!
    var recorder: AVAudioRecorder!
    var levelTimer = Timer()
    let LEVEL_THRESHOLD: Float = -10.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       let documents = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0])
              let url = documents.appendingPathComponent("record.caf")

              let recordSettings: [String: Any] = [
                  AVFormatIDKey:              kAudioFormatAppleIMA4,
                  AVSampleRateKey:            44100.0,
                  AVNumberOfChannelsKey:      2,
                  AVEncoderBitRateKey:        12800,
                  AVLinearPCMBitDepthKey:     16,
                  AVEncoderAudioQualityKey:   AVAudioQuality.max.rawValue
              ]

              let audioSession = AVAudioSession.sharedInstance()
              do {
                try audioSession.setCategory(AVAudioSession.Category.playAndRecord)
                  try audioSession.setActive(true)
                  try recorder = AVAudioRecorder(url:url, settings: recordSettings)

              } catch {
                  return
              }

              recorder.prepareToRecord()
              recorder.isMeteringEnabled = true
              recorder.record()

              levelTimer = Timer.scheduledTimer(timeInterval: 0.02, target: self, selector: #selector(levelTimerCallback), userInfo: nil, repeats: true)

    }
    
    @objc func levelTimerCallback() {
        recorder.updateMeters()

        let level = recorder.averagePower(forChannel: 0)
        let isLoud = level > LEVEL_THRESHOLD

        let keras = isLoud ? "keras" : "tidak keras"
        print("level \(level) isloud \(isLoud)")
        tulisan.text = "level suara: \(level) -- \(keras)"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



}

